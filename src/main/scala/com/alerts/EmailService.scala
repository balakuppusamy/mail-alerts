package com.alerts

import org.apache.commons.mail._

import scala.util.Properties

case class Mail(from: String, to: String, subject: String, message: String)

class EmailService {

  var hostName: String = Properties.envOrElse("MAIL_HOST", "icct-com.mail.protection.outlook.com")

  def sendMail(mail: Mail) {
    val commonsMail: Email = new SimpleEmail()
    commonsMail.setHostName(hostName)
    commonsMail.setFrom(mail.from)
    commonsMail.addTo(mail.to)
    commonsMail.setSubject(mail.subject)
    commonsMail.setMsg(mail.message)
    commonsMail.send()
  }

}

object EmailService {
  def main(args: Array[String]) {
    println("EmailService starts")
    val mail = new Mail("bkuppusamy@icct.com", "bkuppusamy@icct.com", "Alert Mail", "Alert message")
    println("Started to send the alerts mail")
    new EmailService().sendMail(mail)
    println("Successfully sent the alert mail")
  }
}