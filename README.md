This project contains a scala program to send alerts mail. 

To test this, follow the below steps,
1. Install Scala and SBT 
2. Download this repository and update 'receipent' email address in the EmailService scala file.
3. Run 'sbt assembly'
4. Then run 'java -jar ./target/scala-2.12/mail-alerts-assembly-1.0.jar

The EmailService.scala will be extracted out along with the dependency(commons-email) and it will be used in the Spark streaming job to send the alerts.